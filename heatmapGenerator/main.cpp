#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <math.h>
#include <opencv2/optflow.hpp>
#include <cstdlib>
#include <algorithm>
#include <opencv2/xfeatures2d.hpp>
#include <cmath>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string>

using namespace std;
using namespace cv;

//global methods

vector<vector<Point2f> > getLandmarksList(string path);
vector<int> getPatterns(string path);
double distanceBetween2points(Point2f p1, Point2f p2);
vector<double> getLandmarksImportance(string path);

void heatmaps_per_animation();
void heatmaps_per_expression();
void heatmaps_per_expression_and_animation();
void heatmaps_landmarks_importance();

void construct_per_expression_global_image();
void construct_per_animation_global_image();
void construct_per_expression_and_animation_global_image();

//global variables

double error_factor = 0.04;
string pwd = "."; //Path to face_land.csv
string savepath = "heatmaps/"; //Path to where heatmaps should be stored
string datafinder = "data/"; //Path from where to read landmarks and expression patterns segmentation

vector<string> subjects={"SN001", "SN002", "SN003", "SN004","SN005",
	"SN006", "SN007", "SN008", "SN009","SN010",
	"SN011", "SN012", "SN013", "SN014","SN015"};
vector<string> animations={ "Nothing", "Tx", "Roll", "Yaw", "Pitch", "Diag" };
vector<string> expressions={ "Neutral", "Happy", "Fear", "Sad", "Surprise","Anger", "Disgust" };
vector<string> configurations={ "Nothing", "Diag", "Pitch", "Yaw", "Roll", "Tx",
								"Neutral", "Happy", "Fear", "Sad", "Surprise","Anger", "Disgust" };

vector<string> approaches={ "glass", "tcdcn", "dan", "fhr", "sbr", "san" };


int main(int argc, char *argv[]) {


	printf("heatmaps_per_expression...\n");
	heatmaps_per_expression();
	construct_per_expression_global_image();

	printf("heatmaps_per_animation...\n");
	heatmaps_per_animation();
	construct_per_animation_global_image();

	printf("heatmaps_per_expression_and_animation...\n");
	heatmaps_per_expression_and_animation();
	construct_per_expression_and_animation_global_image();

	printf("heatmaps_landmarks_importance...\n");
	heatmaps_landmarks_importance();
	return 0;
}

void construct_per_expression_global_image() {
	int pad=1;
	Mat big_image(approaches.size()*(128+pad),expressions.size()*(100+pad),CV_8UC3);

	for (int exp_no=0;exp_no<expressions.size();exp_no++) {
		string exp=expressions[exp_no];
		for (int app_no=0;app_no<approaches.size();app_no++) {
			string app=approaches[app_no];
			string image_path=savepath + "heatmap_expression" + "/" + app + "_"
					+ "Nothing" + "_" + exp + ".png";
			printf("loading %s and drawing to %d %d\n",image_path.c_str(),exp_no*(100+pad),app_no*(128+pad));
			Mat image=imread(image_path.c_str());
			resize(image,image,Size(100,128),0, 0, CV_INTER_LINEAR);
			image.copyTo(big_image(Rect(exp_no*(100+pad),app_no*(128+pad),image.cols,image.rows)));
		}
	}
	string big_image_path=savepath + "heatmap_all_expressions.png";
	imwrite(big_image_path.c_str(),big_image);
}

void construct_per_expression_and_animation_global_image() {
	int pad=1;
	Mat big_image(approaches.size()*(128+pad),(100+pad),CV_8UC3);

	for (int app_no=0;app_no<approaches.size();app_no++) {
		string app=approaches[app_no];
		string image_path=savepath + "heatmap_expression_pose" + "/" + app + ".png";
		printf("loading %s and drawing to %d %d\n",image_path.c_str(),0,app_no*(128+pad));
		Mat image=imread(image_path.c_str());
		resize(image,image,Size(100,128),0, 0, CV_INTER_LINEAR);
		image.copyTo(big_image(Rect(0,app_no*(128+pad),image.cols,image.rows)));
	}
	string big_image_path=savepath + "heatmap_all_expressions_and_animations.png";
	imwrite(big_image_path.c_str(),big_image);
}

void construct_per_animation_global_image() {
	int pad=1;
	Mat big_image(approaches.size()*(128+pad),animations.size()*(100+pad),CV_8UC3);

	for (int ani_no=0;ani_no<animations.size();ani_no++) {
		string ani=animations[ani_no];
		for (int app_no=0;app_no<approaches.size();app_no++) {
			string app=approaches[app_no];
			string image_path=savepath + "heatmap_pose" + "/" + app + "_"
					+ ani + "_Neutral.png";
			printf("loading %s and drawing to %d %d\n",image_path.c_str(),ani_no*(100+pad),app_no*(128+pad));
			Mat image=imread(image_path.c_str());
			resize(image,image,Size(100,128),0, 0, CV_INTER_LINEAR);
			image.copyTo(big_image(Rect(ani_no*(100+pad),app_no*(128+pad),image.cols,image.rows)));
		}
	}
	string big_image_path=savepath + "heatmap_all_animations.png";
	imwrite(big_image_path.c_str(),big_image);
}

Mat generate_facial_heatmap(vector<Point2f> landmark_heatmap, vector<double> error_global, double diag_mean) {
	Mat image = imread(pwd + "/face.png");
	for (int i = 0; i < landmark_heatmap.size() - 1; i++)
		circle(image, landmark_heatmap[i], diag_mean * error_factor * error_global[i],
				Scalar(255 * (1 - (error_global[i]) * 2), 0,
						255 * (error_global[i] * 3)), -1);
	return image;
}

void aggregate_and_average_errors(vector<vector<double>> &errors, int avg_factor, vector<double> &normalized_errors) {
	double val;
	for (int i = 0; i < errors[0].size(); i++) {
		val = 0;
		for (int j = 0; j < errors.size(); j++) {
			val += errors[j][i];
		}
		normalized_errors[i] = val
				/ avg_factor;
	}
}

void heatmaps_landmarks_importance() {
	vector<vector<Point2f> > landmarks_heatmap = getLandmarksList(
			pwd + "/face_land.csv");

	string name;

	for (int i = 0; i < configurations.size(); i++) {
		if (i > 5)
			name = "importance_expression/landmarks_importance_" + configurations[i]
					+ ".png";
		else
			name = "importance_animation/landmarks_importance_" + configurations[i]
					+ ".png";

		vector<double> importance(getLandmarksImportance(
				datafinder + "dataset/Weights/weights_landmarks_" + configurations[i] + ".txt"));

		vector<double> error_global(68, 0);

		for (int i = 0; i < importance.size(); i++)
			error_global[i] = (importance[i] - 0) / (68 - 0);

		Mat image = generate_facial_heatmap(landmarks_heatmap[0],error_global,10/error_factor);
		/*imread(pwd + "/face.png");
		for (int i = 0; i < landmarks_heatmap[0].size() - 1; i++)
			circle(image, landmarks_heatmap[0][i], 10 * error_global[i],
					Scalar(255 * (error_global[i] * 3), 0,
							255 * (1 - (error_global[i]) * 2)), -1);
		 */
		imwrite(savepath + name, image);
	}
}


vector<double> compute_error_distances(vector<int> &patterns, vector<vector<Point2f>> &landmarks_GT, vector<vector<Point2f>> &landmarks,
					double &diag_sum, double &diag_count) {
	vector<double> error_distance(68, 0);

	for (int img = patterns[1]; img < patterns[3]; img++) {
		printf("dealing with img %d\n",img);

		int min_x=landmarks_GT[img-1][0].x,max_x=landmarks_GT[img-1][0].x;
		int min_y=landmarks_GT[img-1][0].y,max_y=landmarks_GT[img-1][0].y;
		for (int p = 0; p < landmarks[0].size(); p++) {
			min_x=(landmarks_GT[img-1][p].x<min_x)?landmarks_GT[img-1][p].x:min_x;
			max_x=(landmarks_GT[img-1][p].x>max_x)?landmarks_GT[img-1][p].x:max_x;
			min_y=(landmarks_GT[img-1][p].y<min_y)?landmarks_GT[img-1][p].y:min_y;
			max_y=(landmarks_GT[img-1][p].y>max_y)?landmarks_GT[img-1][p].y:max_y;
		}

		int diag=distanceBetween2points(Point2f(min_x,min_y),Point2f(max_x,max_y));
		diag_sum+=diag;diag_count++;

		for (int p = 0; p < landmarks[0].size(); p++) {
			if (distanceBetween2points(landmarks_GT[img - 1][p],
					landmarks[img - 1][p]) >= error_factor * diag)
				error_distance[p] += 1;
			else
				error_distance[p] += distanceBetween2points(
						landmarks_GT[img - 1][p],
						landmarks[img - 1][p]) / (error_factor * diag);
		}
	}

	for (int i = 0; i < error_distance.size(); i++)
		error_distance[i] = error_distance[i]
				/ (double) (abs(patterns[3] - patterns[1]));

	return error_distance;

}



void heatmaps_per_expression() {
	string file = "heatmap_expression";

	string name = datafinder + "error_per_expression.csv";


	vector<vector<Point2f> > landmarks_GT, landmarks_M, landmarks_heatmap;

	landmarks_heatmap = getLandmarksList(pwd + "/face_land.csv");

	vector<vector<double> > errors;

	vector<int> patterns;

	string path = datafinder + "dataset/";

	for (int app = 0; app < approaches.size(); app++) {
		printf("dealing with approach %s\n",approaches[app].c_str());
		for (int e = 0; e < expressions.size(); e++) {
				errors.clear();
				printf("dealing with expressions %s\n",expressions[e].c_str());

				double diag_sum=0, diag_count=0;

				for (int s = 0; s < subjects.size(); s++) {
					printf("dealing with subject %s\n",subjects[s].c_str());
					patterns = getPatterns(
							path + "Patterns/" + subjects[s] + "/"
									+ "Nothing" + "_" + expressions[e]
									+ ".csv");
					landmarks_GT = getLandmarksList(
							path + "Landmarks/cam_gt/" + subjects[s] + "/"
									+ "Nothing" + "_" + expressions[e]
									+ "_cam2.csv");
					landmarks_M = getLandmarksList(
							path + "Landmarks/cam_" + approaches[app] + "/"
									+ subjects[s] + "/" + "Nothing" + "_"
									+ expressions[e] + "_cam2.csv");

					errors.push_back(
							compute_error_distances(patterns,
									landmarks_GT, landmarks_M,
									diag_sum, diag_count)
							);

				}

				vector<double> error_global(68, 0);
				double val;
				for (int i = 0; i < errors[0].size(); i++) {
					val = 0;
					for (int j = 0; j < errors.size(); j++) {
						val += errors[j][i];
					}
					error_global[i] = val / (double) (subjects.size());
				}

				double diag_mean=diag_sum/diag_count;
				Mat image=generate_facial_heatmap(landmarks_heatmap[0],error_global, diag_mean);

				string name = savepath + file + "/" + approaches[app] + "_"
						+ "Nothing" + "_" + expressions[e] + ".png";
				//printf("saving heatmap to %s",name.c_str());
				imwrite(name, image);
		}
	}
}

void heatmaps_per_animation() {
	string file = "heatmap_pose";
	string name = datafinder + "error_per_animation.csv";

	vector<vector<Point2f> > landmarks_GT, landmarks_M, landmarks_heatmap;

	landmarks_heatmap = getLandmarksList(pwd + "/face_land.csv");

	vector<vector<double> > errors;

	vector<int> patterns;

	string path = datafinder + "dataset/";

	for (int app = 0; app < approaches.size(); app++) {
		for (int a = 0; a < animations.size(); a++) {
				errors.clear();
				double diag_sum=0, diag_count=0;

				for (int s = 0; s < subjects.size(); s++) {
					patterns = getPatterns(
							path + "Patterns/" + subjects[s] + "/"
									+ animations[a] + "_" + "Neutral"
									+ ".csv");
					landmarks_GT = getLandmarksList(
							path + "Landmarks/cam_gt/" + subjects[s] + "/"
									+ animations[a] + "_" + "Neutral"
									+ "_cam2.csv");
					landmarks_M = getLandmarksList(
							path + "Landmarks/cam_" + approaches[app] + "/"
									+ subjects[s] + "/" + animations[a] + "_"
									+ "Neutral" + "_cam2.csv");
					errors.push_back(
							compute_error_distances(patterns,
												landmarks_GT, landmarks_M,
												diag_sum, diag_count)
									);
				}

				vector<double> error_global(68, 0);
				aggregate_and_average_errors(errors,subjects.size(),error_global);

				double diag_mean=diag_sum/diag_count;
				Mat image = generate_facial_heatmap(landmarks_heatmap[0],error_global,diag_mean);

				string name = savepath + file + "/" + approaches[app] + "_"
						+ animations[a] + "_" + "Neutral" + ".png";
				imwrite(name, image);

		}
	}
}

void heatmaps_per_expression_and_animation() {
	string file = "heatmap_expression_pose";
	string name = datafinder + "error_per_animation_per_expression.csv";

	vector<vector<Point2f> > landmarks_GT, landmarks_M, landmarks_heatmap;

	landmarks_heatmap = getLandmarksList(pwd + "/face_land.csv");

	vector<vector<double> > errors, errors_expressions, errors_animations;

	vector<int> patterns;

	string path = datafinder + "dataset/";

	string ani, exp;

	double diag_sum=0,diag_count=0;

	for (int app = 0; app < approaches.size(); app++) {
		errors_animations.clear();
		for (int a = 0; a < animations.size(); a++) {
			ani = animations[a];
			errors_expressions.clear();
			for (int e = 0; e < expressions.size(); e++) {
				exp = expressions[e];
				errors.clear();
				for (int s = 0; s < subjects.size(); s++) {
					patterns = getPatterns(
							path + "Patterns/" + subjects[s] + "/"
									+ animations[a] + "_" + expressions[e]
									+ ".csv");
					landmarks_GT = getLandmarksList(
							path + "Landmarks/cam_gt/" + subjects[s] + "/"
									+ animations[a] + "_" + expressions[e]
									+ "_cam2.csv");
					landmarks_M = getLandmarksList(
							path + "Landmarks/cam_" + approaches[app] + "/"
									+ subjects[s] + "/" + animations[a] + "_"
									+ expressions[e] + "_cam2.csv");

					errors.push_back(
									compute_error_distances(patterns,
											landmarks_GT, landmarks_M,
											diag_sum, diag_count)
									);
				}

				vector<double> error_global_expression(68,0);
				aggregate_and_average_errors(errors,subjects.size(),error_global_expression);
				errors_expressions.push_back(error_global_expression);
			}

			vector<double> error_global_animation(68, 0);
			aggregate_and_average_errors(errors_expressions,expressions.size(),error_global_animation);
			errors_animations.push_back(error_global_animation);
		}

		vector<double> error_global(68, 0);
		aggregate_and_average_errors(errors_animations,animations.size(),error_global);

		double diag_mean=diag_sum/diag_count;
		Mat image = generate_facial_heatmap(landmarks_heatmap[0],error_global,diag_mean);
		imread(pwd + "/face.png");
		string name = savepath + file + "/" + approaches[app] + ".png";
		imwrite(name, image);
	}
}

vector<vector<Point2f> > getLandmarksList(string path) {
	ifstream myStream(path.c_str());

	string attribute;
	vector<Point2f> linePoints;
	vector<vector<Point2f> > lands;

	//printf("reading landmarks from %s\n", path.c_str());
	while (!myStream.eof()) {
		linePoints.clear();

		getline(myStream, attribute, ';');
		for (int i = 0; i < 67; i++) {
			getline(myStream, attribute, ';');
			float px = atof(attribute.c_str());
			getline(myStream, attribute, ';');
			float py = atof(attribute.c_str());
			linePoints.push_back(Point(px, py));
		}

		getline(myStream, attribute, ';');
		float px = atof(attribute.c_str());
		getline(myStream, attribute, '\n');
		float py = atof(attribute.c_str());
		linePoints.push_back(Point(px, py));

		lands.push_back(linePoints);
	}

	myStream.close();

	return lands;
}

double distanceBetween2points(Point2f p1, Point2f p2) {
	return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

vector<int> getPatterns(string path) {
	ifstream myStream(path.c_str());

	printf("reading patterns from %s\n", path.c_str());

	string attribute;
	vector<int> patterns;

	while (!myStream.eof()) {
		for (int i = 0; i < 4; i++) {
			getline(myStream, attribute, ';');
			int val = atof(attribute.c_str());
			patterns.push_back(val);
		}

		getline(myStream, attribute, '\n');
		int val = atof(attribute.c_str());
		patterns.push_back(val);
	}

	myStream.close();

	return patterns;
}

vector<double> getLandmarksImportance(string path) {
	ifstream myStream(path.c_str());

	printf("reading landmarks importance from %s\n", path.c_str());

	string attribute;
	vector<double> patterns;

	while (!myStream.eof()) {
		for (int i = 0; i < 67; i++) {
			getline(myStream, attribute, ';');
			int val = atof(attribute.c_str());
			patterns.push_back(val);
		}

		getline(myStream, attribute, '\n');
		int val = atof(attribute.c_str());
		patterns.push_back(val);
	}

	myStream.close();

	return patterns;
}
