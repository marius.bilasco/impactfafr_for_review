This code requires C++11 dialect (GCC > 4.4)

The code uses the entire SNAP-2DFE dataset for the experiments.

The archives includes several parts :


1.  Pattern Data : it contains the annotations concerning the expression activation sequences such as onset, apex, offset.
2.  Landmark Data : landmarks extracted using several face alignment methods on the SNAP-2DFE images recorded using the frontal camera (camera2)
3.  Registration : registration of all images from SNAP-2DFE using the landmarks previously extracted and an implementation of (Hassner et al., 2015) provided by (https://github.com/ChrisYang/facefrontalisation)
4.  Expression Descriptors : LBP and LMP (Allaert et al., 2017) descriptors extracted on the registered images 
5.  Landmark Importance : heatmap generator

== **Pattern Data**

The folder data/dataset/Patterns contains the expression activation sequences (neutral, onset, apex, offset, neutral) for each video in the SNAP-2DFE dataset.

== **Landmark Data**

The folder data/dataset/Landmarks stores the data concerning the landmarks.
The landmark data extracted using the selected face alignment approaches can be downloaded here https://nextcloud.univ-lille.fr/index.php/s/Dyafns4BBfHYXKk and should be placed into data/dataset/Landmarks/

The face alignment methods selected are available on the various github like services here bellow:


1.  FHR = https://github.com/TencentYoutuResearch/FaceAlignment-FHR
2.  SBR = https://github.com/facebookresearch/supervision-by-registration
3.  HG = https://github.com/1adrianb/face-alignment
4.  SAN = https://github.com/D-X-Y/SAN
5.  TCDCN = http://mmlab.ie.cuhk.edu.hk/projects/TCDCN.html (exe)
6.  DAN = https://github.com/MarekKowalski/DeepAlignmentNetwork

The AUC and FR are computed using the methodology indicated in the MENPO challenge :

*  https://www.menpo.org/
*  https://menpofit.readthedocs.io/en/stable/api/menpofit/error/index.html

== **Registration**

We adapted the code from https://github.com/ChrisYang/facefrontalisation in order to be able to use directly the landmarks available in B.

The registration/do_registration_on_all_images.sh scripts launches the face frontalization script on all images recorded using the frontal camera (camera 2) from SNAP-2DFE.

It requires to have the SNAP-2DFE dataset stored on your HDD.

Please provide the location of the dataset. A registered_3d_{FaceAlignmentMethod} folder is constructed by a applying the registration code to all images in the dataset.

== **Expression**

LBP and LMP descriptors were extracted on all registered images.
The descriptors are available for download here :

* LBP - https://nextcloud.univ-lille.fr/index.php/s/SZyCpyRSKsPREox
* LMP - https://nextcloud.univ-lille.fr/index.php/s/Swj8kcdSAn87J5X

Download the desired zip files and unzip them in /data/dataset/Expression

We use libsvm using a 10-fold cross validation protocol using various configurations of descriptors. For instance LBP/FHR/fhr_expr_disgust.csv contains the LBP descriptors for all data obtained using a FHR landmark based registration on all disgust like sequences (including all motions). The LBP/FHR/fhr_mvt_diag corresponds to all LBP descriptors obtained on all data in presence of a diag mouvement regardless of the expression.

== **Heatmap generator**

The code for creating heatmaps is available in heatmapGenerator folder. Before compiling the code, please configure the folder information

It uses the landmarks obtained by various face alignment methods and the groundtruth provided (GT).

It also uses the linear regression coefficients for illustrating the importance of landmarks computed with the excel sheet available in data/dataset/Weights/mean_square_exemple.xlsx