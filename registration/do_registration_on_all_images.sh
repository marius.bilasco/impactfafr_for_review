#!/bin/bash

export LD_LIBRARY_PATH=/usr/local/lib/
orig_data_dir=$1
alignment=$2
dest_data_dir=${orig_data_dir}/registered_3D_$2

rm -rf ${dest_data_dir} 2>/dev/null
mkdir -p ${dest_data_dir}

for S in {1..15}; do

    if [ $S -lt 10 ]
    then
        subject="SN00$S"
    else
        subject="SN0$S"
    fi

    for animation in $(ls ${orig_data_dir}"/images/"$subject"/"); do
        for expression in $(ls ${orig_data_dir}"/images/"$subject"/"$animation"/"); do

            file_pattern=${orig_data_dir}/"/Patterns/"$subject"/"$animation"_"$expression".csv"
            #recupérer onset et apex
            onset=$(cat $file_pattern | cut -d';' -f2)
            apex=$(cat $file_pattern | cut -d';' -f3)
            apex=$(($apex + 1))

            count=$onset
            echo $subject"/"$animation"/"$expression

            for (( i=$onset; i<=$apex; i++ ))
            do

                #count=$(($count + 1))
                count=$i

                if [ $count -le 9 ]
                then
                    image_name="img_00$count.png"
                else
                    image_name="img_0$count.png"
                fi

                image_file=${orig_data_dir}"/images/"$subject"/"$animation"/"$expression"/cam2/"$image_name
                landmarks_file=${orig_data_dir}"/Landmarks/cam_${alignment}/"$subject"/"$animation"_"$expression"_cam2.csv"

                image_save_file=${dest_data_dir}"/"$subject"/"$animation"/"$expression"/"$image_name

                cp $image_file "/tmp/tmp.jpg"

                python register.py "/tmp/tmp.jpg" $landmarks_file $count

                cp "/tmp/tmp.jpg" $image_save_file

                echo $count"/74"

            done
        done
    done
done
